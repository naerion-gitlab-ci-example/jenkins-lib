#!/usr/bin/env groovy

def call(String path) {
    sh 'cd '+path+' mvn --batch-mode -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN test dependency:copy-dependencies'
}