#!/usr/bin/env groovy

def call(String branch,String version) {
    def SONAR_ARGS=''
    if (branch != 'master') 
    {
        SONAR_ARGS="-Dsonar.branch.target="+branch
    }
    def scannerHome = tool 'SonarQube Scanner 4.2';

    withSonarQubeEnv('SonarQube Server') {
        currentBuild.description = version
        currentBuild.displayName = version
        sh scannerHome+'/bin/sonar-scanner '+SONAR_ARGS+' -Dsonar.branch.name='+branch + ' -Dsonar.projectVersion='+version
    }
}
